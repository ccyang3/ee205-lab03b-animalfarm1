###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

all: animalfarm

animalfarm: main.o cat.o animals.o
	cc main.o cat.o animals.o -o animalfarm 

main.o:  cat.h animals.h

cat.o: cat.h animals.h

animals.o: animals.h

clean:
	rm -f *.o animalfarm
