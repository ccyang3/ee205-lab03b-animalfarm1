///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author Christian Yang <ccyang3@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   03/02/21
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"
#include "animals.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat catDB[MAX_SPECIES];

/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {   

   struct Cat Alice;

   strcpy( Alice.name, "Alice"); 
   Alice.gender = FEMALE;
   Alice.catbreeds = MAIN_COON;
   Alice.isFixed = 1;
   Alice.weight = 12.34;
   Alice.collar1_color = BLACK;
   Alice.collar2_color = RED;
   Alice.license = 12345;

   catDB[i] = Alice;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   // Here's a clue of what one printf() might look like...
   printf ("Cat name = [%s]\n", catDB[i].name);
   if(catDB[i].gender == MALE)
      printf ("    gender = [Male]\n");
   else
      printf ("    gender = [Female]\n");

   switch(catDB[i].catbreeds){
      case 0: printf ("    breed = [%s]\n", "Main Coon");
              break;
      case 1: printf ("    breed = [%s]\n", "Manx");
              break;
      case 2: printf ("    breed = [%s]\n", "Shorthair");
              break;
      case 3: printf ("    breed = [%s]\n", "Persian");
              break;
      case 4: printf ("    breed = [%s]\n", "Sphynx");
              break;
      default: break;
      }


   if(catDB[i].isFixed == 0)
      printf ("    isFixed = [No]\n");
   else
      printf ("    isFixed = [Yes]\n");
   printf ("    weight = [%f]\n", catDB[i].weight);
   printf ("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf ("    collar color 2 = [%s]\n", colorName( catDB[i].collar2_color ));
   printf ("    license = [%ld]\n", catDB[i].license);
}

