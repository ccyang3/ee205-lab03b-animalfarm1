///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Christian Yang <ccyang3@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo 03/02/21
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"


/// Decode the enum Color into strings for printf()
char* colorName (int  c) {

   char* colorStr;
   // @todo Map the enum Color to a string
   switch(c){
      case 0: colorStr = "Black";
              break;
      case 1: colorStr = "White";
              break;
      case 2: colorStr = "Red";
              break;
      case 3: colorStr = "Blue";
              break;
      case 4: colorStr = "Green";
              break;
      case 5: colorStr = "Pink";
              break;
      default:  return NULL; // We should never get here
            break;
      }

   return colorStr;
}


